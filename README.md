# 960 Grid System #

A simple and customizable 960px based grid system inspired by http://960.gs.

### What is this repository for? ###

* The 960 pixel based non-responsive grid system written in SASS
* 0.0.1

### How do I get set up? ###

* Download the package (git clone git@bitbucket.org:richi18007/960-gs.git)
* Add link rel="css/style.css" to include the grid template
* It would be better to include normalize.css after style.css (Though optional)
* In case you want to make some changes to the SASS file for the grid , use a sass compiler


### Who do I talk to? ###

* Srivastava , Sankalp < <richi18007@gmail.com> >

License : 
FreeBSD